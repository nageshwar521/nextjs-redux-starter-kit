import axios from "axios";

const getHeaders = (additionalHeaders) => {
  let headers = {
    Accept: "application/json"
  };
  if (typeof localStorage !== "undefined") {
    headers = {
      ...headers,
      // Authorization: "auth here"
    };
  }
  return Object.assign({}, headers, additionalHeaders);
};

const getDefaultAttributes = (headers, ...props) => ({
  cache: true,
  headers: getHeaders(headers),
  ...props
});

export function postFetch({url, data, props}) {
  return new Promise((resolve, reject) => {
    axios
      .post(url, data, getDefaultAttributes(props))
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else {
          reject({ error: true });
        }
      })
      .catch(e => reject(e));
  });
}

export function putFetch({url, data, props}) {
  return new Promise((resolve, reject) => {
    axios
      .put(url, data, getDefaultAttributes(props))
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else {
          reject({ error: true });
        }
      })
      .catch(e => reject(e));
  });
}

export function getFetch({url, props}) {
  return new Promise((resolve, reject) => {
    axios
      .get(url, getDefaultAttributes(props))
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else {
          reject({ error: true });
        }
      })
      .catch(e => reject(e));
  });
}

export function deleteFetch({url, props}) {
  return new Promise((resolve, reject) => {
    axios
      .delete(url, getDefaultAttributes(props))
      .then(res => {
        if (res.status === 200) {
          resolve(res.data);
        } else {
          reject({ error: true });
        }
      })
      .catch(e => reject(e));
  });
}
